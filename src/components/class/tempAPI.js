import React from 'react';
import axios from 'axios';
import { Card } from 'antd';
import { Row, Col } from 'antd';

class TempAPI extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            temprature: -999,
            humidity: -999
          };
    };

    componentDidMount() {
        this.getTemp();
        setTimeout(function() {
            this.getHumi();
        }.bind(this), 2000)
    }


    getTemp = () => {
        axios({
            method: 'GET',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
            },
            url: 'http://192.168.0.26:5000/api/temp/',
        })
        .then(res => this.setState({ temprature: res.data }));
    }

    getHumi = () => {
        axios({
            method: 'GET',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
            },
            url: 'http://192.168.0.26:5000/api/humi/',
        })
        .then(res => this.setState({ humidity: res.data }));
    }


    render() {
        return (
            <div>
                <Card style={{ width: 300 }}>
                    <Row>
                        <Col span={12}>
                            <span>Temprature</span>
                            <p>{this.state.temprature}*</p>
                        </Col>
                        <Col span={6}>
                            <span>Humidity</span>
                            <p>{this.state.humidity}</p>
                        </Col>
                    </Row>
                </Card>
            </div>  
        );
    }

}

export default TempAPI