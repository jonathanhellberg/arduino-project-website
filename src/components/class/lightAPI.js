import React from 'react';
import axios from 'axios';
import qs from 'qs';
import { Switch } from 'antd';
import { Card } from 'antd';
import { Row, Col } from 'antd';
import { BulbOutlined, BulbTwoTone } from '@ant-design/icons';
import { Divider } from 'antd';

class LightAPI extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            greenLight: false,
            redLight: false,
            yellowLight:false
          };
    };

    green = () => {
        axios({
            method: 'post',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
            },
            url: 'http://192.168.0.26:5000/api/send/',
            data: qs.stringify({
                id: "6"
            })
        })
        .then(res => console.log(res))
        this.setState(prevState => ({
            greenLight: !prevState.greenLight
        }));
    }

    yellow = () => {
        axios({
            method: 'post',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
            },
            url: 'http://192.168.0.26:5000/api/send/',
            data: qs.stringify({
                id: "5"
            })
        })
        .then(res => console.log(res))
        this.setState(prevState => ({
            yellowLight: !prevState.yellowLight
        }));
    }

    red = () => {
        axios({
            method: 'post',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
            },
            url: 'http://192.168.0.26:5000/api/send/',
            data: qs.stringify({
                id: "3"
            })
        })
        .then(res => console.log(res))
        this.setState(prevState => ({
            redLight: !prevState.redLight
        }));
    }

    test = () => {
    
    }

    render() {
        return (
            <div>
                <h2>LED Light</h2>
                <Divider />
                { !this.state.greenLight ? (
                    <button onClick={this.green}>Green Off</button>
                ) : (
                    <button onClick={this.green}>Green On</button>
                )}

                { !this.state.yellowLight ? (
                    <button onClick={this.yellow}>Yellow Off</button>
                ) : (
                    <button onClick={this.yellow}>Yellow On</button>
                )}

                { !this.state.redLight ? (
                    <button onClick={this.red}>Red Off</button>
                ) : (
                    <button onClick={this.red}>Red On</button>
                )}

                <Card style={{ width: 300 }}>
                    <Row>
                        <Col span={12}>
                            <span>Green Light</span>
                        </Col>
                        <Col span={6}>
                            { !this.state.greenLight ? (
                                <BulbOutlined />
                            ) : (
                                <BulbTwoTone />
                            )}
                        </Col>
                        <Col span={6}>
                            <Switch onChange={this.green} />
                        </Col>
                    </Row>
                </Card>

                <Card style={{ width: 300 }}>
                    <Row>
                        <Col span={12}>
                            <span>Yellow Light</span>
                        </Col>
                        <Col span={6}>
                            { !this.state.yellowLight ? (
                                <BulbOutlined />
                            ) : (
                                <BulbTwoTone />
                            )}
                        </Col>
                        <Col span={6}>
                            <Switch onChange={this.yellow} />
                        </Col>
                    </Row>
                </Card>

                <Card style={{ width: 300 }}>
                    <Row>
                        <Col span={12}>
                            <span>Red Light</span>
                        </Col>
                        <Col span={6}>
                            { !this.state.redLight ? (
                                <BulbOutlined />
                            ) : (
                                <BulbTwoTone />
                            )}
                        </Col>
                        <Col span={6}>
                            <Switch onChange={this.red} />
                        </Col>
                    </Row>
                </Card>
            </div>  
        );
    }

}

export default LightAPI